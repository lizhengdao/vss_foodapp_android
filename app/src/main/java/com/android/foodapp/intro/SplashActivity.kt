package com.android.foodapp.intro

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.android.foodapp.R
import com.android.foodapp.utils.goActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(Runnable {
            this.goActivity(IntroActivity::class.java)
            finish()
        }, 3000)

    }
}
