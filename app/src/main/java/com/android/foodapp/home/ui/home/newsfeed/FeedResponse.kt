package com.android.foodapp.home.ui.home.newsfeed

data class FeedResponse(
    val data: List<Data>
)